#include "lab_JavaCppRunner.h"
#include "jni.h"
#include "jni_md.h"

/**
*	Desaturacja
*/
JNIEXPORT jobjectArray JNICALL 
Java_lab_JavaCppRunner_desaturation (JNIEnv *env, jobject obj, jobjectArray pixelArray){

jint width = env->GetArrayLength(pixelArray);
	
	for (jint i = 0; i < width; i++)
	{
		jobjectArray dimension = (jobjectArray)env->GetObjectArrayElement(pixelArray, i);
		jint* oneRow = env->GetIntArrayElements((jintArray)dimension, false);

		jint height = env->GetArrayLength(dimension);
		for (jint j = 0; j < height; ++j) {
		
			jint pixel = oneRow[j];

			jint r = (pixel >> 16) & 0xFF;
			jint g = (pixel >> 8) & 0xFF;
			jint b = pixel & 0xFF;
			
			jint gray = (r + g + b) / 3;

			oneRow[j] = (gray << 16) + (gray << 8) + gray;
		}

		env->SetIntArrayRegion((jintArray)dimension, 0, height, oneRow);
	}
	return pixelArray;
}

jint enlighten(jint color, jint enlighenLevel){
	color += enlighenLevel;
	
	if (color > 255) {
		color = 255;
	}
	else if (color < 0) {
		color = 0;
	}
	return color;
}

/**
*	Rozjasnianie
*/
JNIEXPORT jobjectArray JNICALL 
Java_lab_JavaCppRunner_enlightening(JNIEnv *env, jobject obj, jobjectArray pixelArray, jobject callbackObj){

	jclass myClass = env->GetObjectClass(callbackObj);
	jmethodID callback = env->GetMethodID(myClass, "getEnlightenLevel", "()I");

	jint enlightenLevel = env->CallIntMethod(callbackObj, callback);
	
	jint width = env->GetArrayLength(pixelArray);
	
	for (jint i = 0; i < width; i++)
	{
		jobjectArray dimension = (jobjectArray)env->GetObjectArrayElement(pixelArray, i);
		jint* oneRow = env->GetIntArrayElements((jintArray)dimension, false);

		jint height = env->GetArrayLength(dimension);
		for (jint j = 0; j < height; ++j) {
		
			jint pixel = oneRow[j];

			jint r = enlighten((pixel >> 16) & 0xFF, enlightenLevel);
			jint g = enlighten((pixel >> 8) & 0xFF, enlightenLevel);
			jint b = enlighten((pixel & 0xFF), enlightenLevel);

			oneRow[j] = (r << 16) + (g << 8) + b;
		}

		env->SetIntArrayRegion((jintArray)dimension, 0, height, oneRow);
	}
	return pixelArray;
}

