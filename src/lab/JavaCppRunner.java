package lab;

import static java.lang.System.getProperty;
import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;
import static javax.swing.JFileChooser.APPROVE_OPTION;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.imageio.ImageIO;
import javax.management.InstanceAlreadyExistsException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class JavaCppRunner {

	private Map<String, List<String>> files = new HashMap<>();
	private static ImageProcessor imageProcessor;

	private native int[][] desaturation(int[][] pixelArray);

	private native int[][] enlightening(int[][] pixelArray, EnlightenLevelCallback callback);

	static {

		System.loadLibrary("cppRunner");
	}

	public static void main(String[] args) throws Exception {

		new JavaCppRunner();
	}

	public JavaCppRunner() throws Exception{
		
		MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
		ObjectName mxBeanName = new ObjectName("lab:type=ImageProcessorImpl");
		
		imageProcessor = new ImageProcessorImpl(this);
		
		mbs.registerMBean(imageProcessor, mxBeanName);
		
		JFrame frame = new JFrame();
		frame.getContentPane().setLayout(null);
		frame.setSize(new Dimension(170, 177));

		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(new File(getProperty("user.home")));

		JButton btnLoadFile = new JButton("Rozjaśnianie");
		btnLoadFile.setBounds(12, 13, 141, 25);
		btnLoadFile.addActionListener(e -> runEnlightenment(btnLoadFile, fileChooser));
		frame.getContentPane().add(btnLoadFile);

		JButton btnRun = new JButton("Desaturacja");
		btnRun.setBounds(12, 67, 141, 25);
		frame.getContentPane().add(btnRun);
		btnRun.addActionListener(e -> runDesaturation(btnRun, fileChooser));

		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		frame.setVisible(true);
	}

	private Optional<String> selectPathToFile(JButton btnRun, JFileChooser fileChooser) {

		int selected = fileChooser.showOpenDialog(btnRun);

		if (selected == APPROVE_OPTION) {
			return ofNullable(fileChooser.getSelectedFile().getAbsolutePath());
		}

		return empty();
	}

	private void runDesaturation(JButton btnRun, JFileChooser fileChooser) {

		String outputFileName = "desaturation_result";

		selectPathToFile(btnRun, fileChooser).ifPresent(fullPath -> performDesaturation(fullPath, outputFileName));
	}

	void performDesaturation(String fullPath, String outputFileName) {

		try {
			BufferedImage img = ImageIO.read(new File(fullPath));

			int[][] result = desaturation(getPixelArray(img));

			createNewImage(img, result);

			File outputFile = new File(outputFileName);

			ImageIO.write(img, "jpg", outputFile);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void createNewImage(BufferedImage img, int[][] result) {

		for (int i = 0; i < img.getWidth(); i++) {
			for (int j = 0; j < img.getHeight(); j++) {
				img.setRGB(i, j, result[i][j]);
			}
		}
	}

	private int[][] getPixelArray(BufferedImage img) {

		int[][] pixels = new int[img.getWidth()][img.getHeight()];

		for (int i = 0; i < img.getWidth(); i++)
			for (int j = 0; j < img.getHeight(); j++)
				pixels[i][j] = img.getRGB(i, j) & 0x00FFFFFF;

		return pixels;
	}

	private void runEnlightenment(JButton btnLoadClass, JFileChooser fileChooser) {

		String outputFileName = "englightenment_result";

		selectPathToFile(btnLoadClass, fileChooser)
				.ifPresent(fullPath -> performEnlightenment(fullPath, outputFileName));
	}

	void performEnlightenment(String fullPath, String outputFileName) {
		
		try {
			BufferedImage img = ImageIO.read(new File(fullPath));

			int[][] result = enlightening(getPixelArray(img), EnlightenLevelCallback.getInstance());

			createNewImage(img, result);

			File outputFile = new File(outputFileName);

			ImageIO.write(img, "jpg", outputFile);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
