package lab;

import static javax.swing.JOptionPane.showMessageDialog;

public class ImageProcessorImpl implements ImageProcessor{
	
	private JavaCppRunner cppRunner;

	public ImageProcessorImpl() {}
	
	public ImageProcessorImpl(JavaCppRunner cppRunner){
		this.cppRunner = cppRunner;
	}
	
	@Override
	public void enlightenWithSurprise(String fullPath, String outputFileName) {

		cppRunner.performEnlightenment(fullPath, outputFileName);
	}

	@Override
	public void silentDesaturation(String fullPath, String outputFileName) {

		cppRunner.performDesaturation(fullPath, outputFileName);
	}

	@Override
	public void showMessage(String message) {
		
		showMessageDialog(null, message);
	}

}
