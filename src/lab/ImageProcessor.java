package lab;

import javax.management.MXBean;

@MXBean
public interface ImageProcessor {

	void enlightenWithSurprise(String fullPath, String outputFileName);
	void silentDesaturation(String fullPath, String outputFileName);
	void showMessage(String message);
}
