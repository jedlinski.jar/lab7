package lab;

import java.beans.ConstructorProperties;

public class FileProcessorParameters {

	private String inputFile;
	private String outputFile;
	
	public FileProcessorParameters() {
	}
	
	@ConstructorProperties({"inputFile", "outputFile"})
	public FileProcessorParameters(String inputFile, String outputFile){
		this.inputFile = inputFile;
		this.outputFile = outputFile;
	}
	
	public String getInputFile() {
		return inputFile;
	}

	public String getOutputFile() {
		return outputFile;
	}

}
