package lab;

import static java.lang.Integer.parseInt;
import static javax.swing.JOptionPane.showInputDialog;

public class EnlightenLevelCallback {

	private static EnlightenLevelCallback instance = new EnlightenLevelCallback();
	
	private EnlightenLevelCallback(){};
	
	public static EnlightenLevelCallback getInstance(){
		return instance;
	}
	
	public int getEnlightenLevel(){
		return parseInt(showInputDialog("Podaj stopień rozjaśnienia/przyciemnienia"));
	}
}
